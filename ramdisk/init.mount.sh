#!/res/busybox sh

export PATH=/res/asset:$PATH

mount -t ext4 -o ro,noatime,nodiratime,data=ordered,barrier=0,discard /dev/block/platform/msm_sdcc.1/by-name/system /system
mount -t f2fs -o ro,noatime,nodiratime,discard /dev/block/platform/msm_sdcc.1/by-name/system /system

if [ -f /system/framework/multiwindow.jar ] ; then
	umount -f /system
	mount -t ext4 -o noatime,nodiratime,data=ordered,barrier=0,discard,nosuid,nodev,noauto_da_alloc,journal_async_commit,errors=panic /dev/block/platform/msm_sdcc.1/by-name/userdata /arter97/data
	mount -t f2fs -o noatime,nodiratime,discard,nosuid,nodev /dev/block/platform/msm_sdcc.1/by-name/userdata /arter97/data
	rm /arter97/data/arter97_secondrom/system/lib/modules
	rm -rf /arter97/data/arter97_secondrom/system/lib/modules/*
	ln -s /lib/modules/dhd.ko /arter97/data/arter97_secondrom/system/lib/modules/dhd.ko
	chmod 755 /arter97/data/arter97_secondrom/system
	chmod 771 /arter97/data/arter97_secondrom/data
	chmod 771 /arter97/data/arter97_secondrom/cache
	mount --bind /arter97/data/arter97_secondrom/system /system
	mount --bind /arter97/data/arter97_secondrom/data /data
	mount --bind /arter97/data/arter97_secondrom/cache /cache
	mkdir /arter97/data/.arter97
	mkdir /data/.arter97
	rm -rf /data/.arter97/*
	rm -rf /data/.arter97/.*
	chmod 777 /arter97/data/.arter97
	chmod 777 /data/.arter97
	mount --bind /arter97/data/.arter97 /data/.arter97
	mount --bind -o remount,suid,dev /system
	if [ -f /arter97/data/media/0/.arter97/shared ]; then
		rm -rf /arter97/data/arter97_secondrom/data/media/0/.arter97
		cp -rp /arter97/data/arter97_secondrom/data/media/* /arter97/data/media/
		rm -rf /data/media/*
		mount --bind /arter97/data/media /data/media
	fi
	CUR_PATH=$PATH
	export PATH=/sbin:/system/sbin:/system/bin:/system/xbin
	export LD_LIBRARY_PATH=/vendor/lib:/system/lib
	run-parts /arter97/data/arter97_secondrom/init.d
	export PATH=$CUR_PATH
else
	rm -rf /arter97
	mount -t ext4 -o noatime,nodiratime,data=ordered,barrier=0,discard,nosuid,nodev,noauto_da_alloc,journal_async_commit,errors=panic /dev/block/platform/msm_sdcc.1/by-name/userdata /data
	mount -t f2fs -o noatime,nodiratime,discard,nosuid,nodev /dev/block/platform/msm_sdcc.1/by-name/userdata /data
	mount -t ext4 -o noatime,nodiratime,data=ordered,barrier=0,discard,nosuid,nodev,noauto_da_alloc,journal_async_commit,errors=panic /dev/block/platform/msm_sdcc.1/by-name/cache /cache
	mount -t f2fs -o noatime,nodiratime,discard,nosuid,nodev /dev/block/platform/msm_sdcc.1/by-name/cache /cache
fi
